# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.0

- minor: Support for Code Insights

## 1.0.0

- major: Initial commit

