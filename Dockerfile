FROM python:3.7-slim
RUN apt-get update && apt-get install -y git
RUN git --version
COPY requirements.txt /
RUN pip install -r /requirements.txt

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["python3", "/pipe.py"]
